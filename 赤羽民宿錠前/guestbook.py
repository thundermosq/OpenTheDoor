
#!/usr/bin/env python

# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START imports]
import os
import urllib

from google.appengine.ext import ndb

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
# [END imports]

a=0
DEFAULT_GUESTBOOK_NAME = 'default_guestbook'
DEFAULT_ACTION = 'default_action'
DEFAULT_IDENTITY = 'default_identity'


# We set a parent key on the 'Greetings' to ensure that they are all
# in the same entity group. Queries across the single entity group
# will be consistent. However, the write rate should be limited to
# ~1/second.

def guestbook_key(guestbook_name=DEFAULT_GUESTBOOK_NAME):
    """Constructs a Datastore key for a Guestbook entity.

    We use guestbook_name as the key.
    """
    return ndb.Key('Guestbook', guestbook_name)


# [START greeting]
class Author(ndb.Model):
    """Sub model for representing an author."""
    identity = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class Greeting(ndb.Model):
    """A main model for representing an individual Guestbook entry."""
    author = ndb.StructuredProperty(Author)
    content = ndb.StringProperty(indexed=False)
    date = ndb.DateTimeProperty(auto_now_add=True)
# [END greeting]

# [START greeting]
class Api(ndb.Model):
    """Sub model for representing an author."""
    #identity = ndb.StringProperty(indexed=False)
    action = ndb.StringProperty(indexed=False)
    #result = ndb.StringProperty(indexed=False)

# [END greeting]


# [START main_page]
class MainPage(webapp2.RequestHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Its works.')
# [END main_page]

# [START gui]
class Gui(webapp2.RequestHandler):

    def get(self):
        action = self.request.get('action', DEFAULT_ACTION)
        identity = self.request.get('id', DEFAULT_IDENTITY)

        if action == 'open_the_door':
            template_values = {
                'id': identity,
                'action': action,
            }

            template = JINJA_ENVIRONMENT.get_template('openthedoor.html')
            self.response.write(template.render(template_values))

        else:
            self.response.headers['Content-Type'] = 'text/plain'

# [END gui]

# [START search]

class Search(webapp2.RequestHandler):

    def get(self):
        action = self.request.get('action', DEFAULT_ACTION)
        identity = self.request.get('id', DEFAULT_IDENTITY)
        self.response.headers['Content-Type'] = 'text/plain'
        global a
        if action == 'open_the_door':
            self.response.write(a)
            a=0
        else:
            self.response.write('deny')

# [END search]


# [START guestbook]
class Do(webapp2.RequestHandler):

    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.
        action = self.request.get('action', DEFAULT_ACTION)
        identity = self.request.get('id', DEFAULT_IDENTITY)
        global a
        if action == 'open_the_door':
            a=1

        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('action:')
        self.response.write(action)
        self.response.write('id:')
        self.response.write(identity)
# [END guestbook]

# [START app]
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/search', Search),
    ('/gui', Gui),
    ('/do', Do),
], debug=True)
# [END app]

package web;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class OpenTheDoor
 */
public class OpenTheDoor extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Logger logger = Logger.getLogger(OpenTheDoor.class.getName());  
    
    /**
     * Default constructor. 
     */
    public OpenTheDoor() {
        
        try {
            logger.setUseParentHandlers(false);
            FileHandler fh = new FileHandler("/data/log/ueno.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);
            logger.info("The server are started.");
        } catch (Exception e) {}
        
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        service();
        logger.info("Some one opened the door.");  
    }
    
    private void service() {
        short port = 4242;
        DatagramSocket socket = null;
        try {
            byte[] buf = "open the door".getBytes();
            socket = new DatagramSocket();
            socket.setBroadcast(true);
            // Maybe the Router does not work.
            // Change to local broadcasting ip ex. 192.168.1.255
            //InetAddress address = InetAddress.getByName("255.255.255.255");
            //InetAddress address = InetAddress.getByName("192.168.1.255");
            //DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
            //socket.send(packet);

            List<InetAddress> broadcastAddress = this.listAllBroadcastAddresses();
            for (InetAddress inetAddress : broadcastAddress) {
                socket.send(new DatagramPacket(buf, buf.length, inetAddress, port));
            }
        } catch (Exception e) {
        } finally {
            if (socket != null)
                socket.close();
        }
    }

    List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<InetAddress>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(a -> a.getBroadcast()).filter(Objects::nonNull)
                    .forEach(broadcastList::add);
        }
        return broadcastList;
    }
}

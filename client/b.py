from evdev import InputDevice, ecodes, list_devices
from select import select

dev = InputDevice("/dev/input/event0")

barcode = ""
while True:
    r,w,x = select([dev], [], [])

    for event in dev.read():
        if event.type == 1 and event.value == 1:
             barcode += str(event.code)

    print(barcode)

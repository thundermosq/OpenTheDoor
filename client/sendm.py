#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
receiver.py

16/May/2018
'''
import threading
import wireman
import requests
import time
#import json

def processing(data):
    while data:
        msg_obj = data.pop(0)
        mode_int = msg_obj['mode']
        red_array = toBin(msg_obj['r'])
        green_array = toBin(msg_obj['g'])
        wireman.start(mode_int, red_array, green_array)

def toBin(s_data):
    result = []
    for b in s_data:
        for a in format(int(b, 16), '0>4b'):
            result.append(int(a))
    return result

def send(bar):
    url = "http://system.jctop.biz:8080/jctop/sms-password=d31f-5g45fc9o-3ci3cjj-3f2d34jif-98342hf-get/"
    pdata = {"qr":"lock001,"+bar}
    r = requests.post(url, data = pdata)
    print (r.text)



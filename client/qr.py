from pynput.keyboard import Key, Listener

class Qr():
    def __init__(self, callback):
        self.__callback = callback
        self.__data = []

    def on_press(self, key):
        print("on_press key:{}".format(key))
        if str(key) != 'Key.enter':
            try:
                self.__data.append(key.char[0:1])
            except AttributeError:
                return
        else:
            qr_code = ''.join(self.__data)
            self.__callback(qr_code)

    def start(self):
        print("start qr reader.")
        with Listener(on_press=self.on_press) as listener:
            listener.join()


def callback(qr_code):
    print("callback qr_code:{}".format(qr_code))
    f = open("demofile2.txt", "a")
    f.write(x)
    f.close()

qr = Qr(callback)
qr.start()

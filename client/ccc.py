from evdev import InputDevice, ecodes, list_devices, categorize
import signal, sys
import requests
import RPi.GPIO as GPIO
import time

SE  = 11    # 1
scancodes = {
    # Scancode: ASCIICode
    0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
    10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
    20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LCTRL',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u';',
    40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
    50: u'M', 51: u',', 52: u'.', 53: u'/', 54: u'RSHFT', 56: u'LALT', 100: u'RALT'
}
barCodeDeviceString = "Metrologic Metrologic Scanner"

devices = map(InputDevice, list_devices())
for device in devices:
    if device.name == barCodeDeviceString:
        dev = InputDevice(device.fn)

def signal_handler(signal, frame):
    print("Stopping")
    dev.ungrab()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
dev = InputDevice("/dev/input/event0")
dev.grab()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
channels = [SE]
GPIO.setup(channels,GPIO.OUT)
GPIO.output(SE, GPIO.LOW)
barcode = ""
for event in dev.read_loop():
    if event.type == ecodes.EV_KEY:
        data = categorize(event)
        if data.keystate == 1 and data.scancode != 42: # Catch only keydown, and not Enter
            if data.scancode == 28:
                #send(barcode)
                url = "http://system.jctop.biz:8080/jctop/sms-password=d31f-5g45fc9o-3ci3cjj-3f2d34jif-98342hf-get"
                pdata = {"qr":"lock001,"+barcode}
                r = requests.post(url, data = pdata)
                
                f = open("test.log","w")
                f.write(r.text)
                f.close()
                if r.text == "ok":
                    GPIO.output(SE, GPIO.HIGH)
                    time.sleep(1)
                    GPIO.output(SE, GPIO.LOW)
                barcode = ""
            else:
                barcode += str(data.scancode)

def send(bar):
    url = "http://system.jctop.biz:8080/jctop/sms-password=d31f-5g45fc9o-3ci3cjj-3f2d34jif-98342hf-get/"
    pdata = {"qr":"lock001,"+bar}
    r = requests.post(url, data = pdata)
    print (r.text)
